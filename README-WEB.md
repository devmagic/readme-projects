# Project Title

One Paragraph of project description goes here

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Quick setup
descrição básica de como rodar o projeto, supondo que tudo já esta configurado.
Exemplo:

* vagrant up
* npm install
* node migration 
* node app.js

### Deafult Branches

* staging : homolog envioroment
* production : production envioroment
* other-branch : use only for test deploy


### Prerequisites

What things you need to install the software and how to install them
Coisas que preciso ter ex: vagrant? node? bash?

```
Give examples
```

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
Give the example
```

### Running 


```
nodemon start ...
ionic serve ...
gulp serve ...
```


```
until finished
```

### DATABASES & API

No casos do backend endereço do BD
- Host: localhost
- Port: 15432
- Database: tdm_local
- Username: tdm
- Password: asd

No caso do app endereço dos endpoints de produção e homolog
- api.meuapp.staging.com
- api.meuapp.production.com


## Running the tests *optional

Explain how to run the automated tests for this system

### Break down into end to end tests *optional

Explain what these tests test and why

```
Give an example
```

## Deployment Documentations
* No android não foi informado a conta para publicar o aplicativo, sendo assim deve ser publicado na conta da devmagic
* Essa aplicação é usada com o perfil do cliente tanto na Apple Store quanto na Google Play, sendo assim sua distribuição é bem peculiar, verifique o login e senha na documentação 
* [Logins do Projeto x](http://www.dropwizard.io/1.0.2/docs/)
* [Deploy App Store](http://www.dropwizard.io/1.0.2/docs/) - Como fazer o deploy de um app para a  App Store
* [Deploy PlayStore](https://maven.apache.org/) - Como fazer o deploy de um app para a PlayStore

## Built and deployment  *required for mobile applications


##### Quick IOS Buiçd

Aplicação deve conter o Bundle ID:  
br.com.myclinic.imoc

* verificar versão da aplicação

Para gerar o Build

1: ionic cordova resources : verificar se já está versionado o resources e icones, caso já esteja não é necessário
2: ionic cordova platform add ios: adiciona a plataforma e instala os plugins
3: ionic cordova prepare ios: atualiza a plataforma com as ultimas alterações do projeto
4: abra o diretório da plataforma e o arquivo Xcode para iniciar o procedimento de deploy

##### Quick Android Build

Aplicação deve conter o Bundle ID:  
br.com.hacklab.imoc

* verificar versão da aplicação

Para gerar o APK:  

1: cordova build --release android

2: jarsigner -tsa http://timestamp.digicert.com -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore mocmobile.keystore /Users/paulocampos/Projects/moc/platforms/android/build/outputs/apk/android-release-unsigned.apk mocmobile

3: **Keystore:** q1qgybG4&ktl

4: **Key:** 37%IkM1kk8GM

5: cd /Users/paulocampos/Library/Android/sdk/build-tools/25.0.0

6: ./zipalign -v 4 /Users/paulocampos/Projects/moc/platforms/android/build/outputs/apk/android-release-unsigned.apk /Users/paulocampos/Projects/moc/platforms/android/build/outputs/apk/moc.apk


## Cofluence & tutorials 

* Please read the doc [COFLUENCE-SPACE.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details about the client and credentials.(adicione o link do espaço do projeto aqui)
* Remote acess to everoments.. [COFLUENCE-SPACE.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details about the client and credentials.(link para documentações especificas)


## Authors & Colaborators

* Billie Thompson - Add yourself here, and what y did 
* Michael Thompson - Initial commit
* Michael Thompson - feats and fixes
* Michael Thompson - generical messages


